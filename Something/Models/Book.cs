﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Something.Models
{
    public class Book
    {
        public Book()
        {
            Quotes = new List<string>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Note { get; set; }
        public ICollection<string> Quotes { get; set; }
        [DataType(DataType.Date)]
        public DateTime? ReadDate { get; set; }
        public string ImagePath { get; set; }
        HttpPostedFileBase ImageUpload { get; set; }
        public ICollection<Category> Categories { get; set; }
    }
}