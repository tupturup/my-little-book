﻿using Something.Models;
using Something.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static Something.ViewModels.HomeIndexViewModel;


namespace Something.Controllers
{
    public class HomeController : Controller
    {
        //start page
        public ActionResult Index()
        {
            HomeIndexViewModel model = new HomeIndexViewModel();
            using (BookContext db = new BookContext())
            {
                model.Books = db.Books.Select(x => new BookVM
                {
                    Id = x.Id,
                    Title = x.Title,
                    Author = x.Author,
                    ImagePath = x.ImagePath
                }).OrderByDescending(o => o.Id).ToList();
            }
                return View(model);
        }

        //adding new book
        [HttpPost]
        public ActionResult Index(Book book, HttpPostedFileBase ImagePath)
        {
            using(BookContext db = new BookContext())
            {
                if (ModelState.IsValid)
                {
                    if (ImagePath != null && ImagePath.ContentLength > 0)
                    { 
                        ImagePath.SaveAs(HttpContext.Server.MapPath("~/Images/") + ImagePath.FileName);
                        book.ImagePath = ImagePath.FileName;

                        //string path = Path.Combine(Server.MapPath("~/Images"), Path.GetFileName(ImagePath.FileName));
                        //ImagePath.SaveAs(path);
                    }

                    //zanemarene kategorije
                db.Books.Add(book);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
            }
        }

        //showing info of a selected book
        public ActionResult Book(int Id)
        {
            HomeBookViewModel model = new HomeBookViewModel();
            using (BookContext db = new BookContext())  
            {
                model.Knjiga = db.Books.Where(x => x.Id == Id).Select(x => new HomeBookViewModel.NekaKnjiga
                {
                    Id = x.Id,
                    Author = x.Author,
                    Note = x.Note,
                    ReadDate = x.ReadDate,
                    Title = x.Title,
                    ImagePath = x.ImagePath,
                }).FirstOrDefault();

                model.Books = db.Books.Select(x => new BookVM {
                    Id = x.Id,
                    Title = x.Title,
                    Author = x.Author,
                    ImagePath = x.ImagePath
                }).OrderByDescending(o => o.Id).ToList();

                return View(model);
            }
        }  

        //editing selected book
        public ActionResult Edit(int Id)
        {
            HomeBookViewModel model = new HomeBookViewModel();
            using (BookContext db = new BookContext())
            {
                model.Knjiga = db.Books.Where(x => x.Id == Id).Select(x => new HomeBookViewModel.NekaKnjiga
                {
                    Id = x.Id,
                    Author = x.Author,
                    Note = x.Note,
                    ReadDate = x.ReadDate,
                    Title = x.Title,
                    ImagePath = x.ImagePath
                }).FirstOrDefault();

                model.Books = db.Books.Select(x => new BookVM
                {
                    Id = x.Id,
                    Title = x.Title,
                    Author = x.Author,
                    ImagePath = x.ImagePath
                }).OrderByDescending(o => o.Id).ToList();

                return View(model);
            }
        }

        //saving changes of the selected book
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id, Author, Note, Title, DateRead, ImagePath")] Book book, HttpPostedFileBase ImagePath)
        {
            using (BookContext db = new BookContext())
            {
                if (ModelState.IsValid)
                {
                    if (ImagePath != null && ImagePath.ContentLength > 0)
                    {
                        ImagePath.SaveAs(HttpContext.Server.MapPath("~/Images/") + ImagePath.FileName);
                        book.ImagePath = ImagePath.FileName;
                    }

                    db.Entry(book).State = EntityState.Modified;
                    //db.Books.Attach(book);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Book", new { Id = book.Id});
        }
        
        //deleting book
        public ActionResult Delete (int Id)
        {
            using (BookContext db = new BookContext())
            {
                Book knj = db.Books.Find(Id);

                if (ModelState.IsValid)
                {
                    db.Books.Remove(knj);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }

        //searching for a keyword
        public ActionResult Search(string searchTerm)
        {
            HomeIndexViewModel model = new HomeIndexViewModel();
            model.searchTerm = searchTerm;

            using (BookContext db = new BookContext())
            {
                model.Books = db.Books.Where(x => x.Title.Contains(searchTerm)).Select(x => new HomeIndexViewModel.BookVM
                {
                    Id = x.Id,
                    Author = x.Author,
                    Title = x.Title
                }).OrderByDescending(o => o.Id).ToList();
            }
            return View("Pretraga", model);
        }

        //search
        [HttpPost]
        public ActionResult Search(Book book, HttpPostedFileBase ImagePath)
        {
            using (BookContext db = new BookContext())
            {
                if (ModelState.IsValid)
                {
                    if (ImagePath != null && ImagePath.ContentLength > 0)
                    {
                        ImagePath.SaveAs(HttpContext.Server.MapPath("~/Images/") + ImagePath.FileName);
                        book.ImagePath = ImagePath.FileName;
                    }
                    //zanemarene kategorije
                    db.Books.Add(book);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
        }
    }
}

