﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Something.ViewModels
{
    public class HomeIndexViewModel
    {
        public HomeIndexViewModel()
        {
            Books = new List<BookVM>();
        }

        public List<BookVM> Books { get; set; }

        public class BookVM {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Author { get; set; }
            public string ImagePath { get; set; }
            HttpPostedFileBase ImageUpload { get; set; }
        }

        public string ImagePath { get; set; }
        HttpPostedFileBase ImageUpload { get; set; }

        public string searchTerm { get; set; }
        
    }
}