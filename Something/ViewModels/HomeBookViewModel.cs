﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Something.ViewModels
{
    public class HomeBookViewModel
    {
        public HomeBookViewModel()
        {
            Books = new List<HomeIndexViewModel.BookVM>();
        }

        public List<HomeIndexViewModel.BookVM> Books { get; set; }

        public NekaKnjiga Knjiga { get; set; }

        public class NekaKnjiga {
            public NekaKnjiga()
            {
                Quotes = new List<string>();
            }

            public int Id { get; set; }
            public string Title { get; set; }
            public string Author { get; set; }
            public string Note { get; set; }
            public ICollection<string> Quotes { get; set; }
            public string ImagePath { get; set; }
            HttpPostedFileBase ImageUpload { get; set; }

            [DataType(DataType.Date)]
            public DateTime? ReadDate { get; set; }
        }
    }
}